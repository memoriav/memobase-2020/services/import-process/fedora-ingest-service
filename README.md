## Fedora Ingest Service

This service reads a kafka topic and ingest each message into Fedora. Each message can contain one or several RDF resources which are 
all created. The service can ingest associated digital files as well.

The service takes care of transformation between local fedora namespaces and the actual namespace used by memobase.

[Confluence Doku](https://ub-basel.atlassian.net/wiki/spaces/MEMOBASE/pages/1891041292/Service+Fedora+Ingest)

### Configuration

- `SFTP_HOST`: The sftp server host name.
- `SFTP_PORT`: The sftp server port.
- `SFTP_USER`: The sftp admin user username.
- `SFTP_PASSWORD`: Password of the admin user.
- `IS_SIMPLE`: Set to true to use the SimpleIngester.
- `FEDORA_INTERNAL_BASE_URL`: The base url of the fedora instance. 
- `FEDORA_EXTERNAL_BASE_URL`: Memobase namespace
- `FEDORA_USER`: Fedora ingest user username
- `FEDORA_PASSWORD`: Fedora ingest user password
- `KAFKA_BOOTSTRAP_SERVERS`: Kafka bootstrap server string.
- `CLIENT_ID`: Unique id for this service deployment.
- `TOPIC_IN`: Input topic in Kafka.
- `TOPIC_OUT`: Not used.
- `TOPIC_REPORTING`: Topic to send the reports to.
