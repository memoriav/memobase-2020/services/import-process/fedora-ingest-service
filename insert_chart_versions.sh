#!/usr/bin/env bash

echo Creating release: $1
sed -i "s/version: 0.0.0/version: $CI_COMMIT_TAG/g" ./helm-charts/Chart.yaml
sed -i "s/appVersion: 0.0.0/appVersion: $CI_COMMIT_TAG/g" ./helm-charts/Chart.yaml
sed -i "s/tag: \"latest\"/tag: $CI_COMMIT_TAG/g" ./helm-charts/values.yaml