package org.memobase

import java.io.File
import java.io.StringWriter
import java.nio.charset.Charset
import org.apache.jena.riot.Lang
import org.apache.jena.riot.RDFDataMgr
import org.apache.logging.log4j.LogManager
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestRdfHandler {
    private val log = LogManager.getLogger("TestRdfHandler")

    private val resourcePath = "src/test/resources/rdf"
    private fun readFile(fileName: String): String {
        return File("$resourcePath/$fileName").readText(Charset.defaultCharset())
    }

    private val regex = Regex("(_:B[A-Za-z0-9]+)")

    private fun sort(input: List<String>): String {
        return input.map {
            var replacedString = it
            for (matchResult in regex.findAll(it)) {
                replacedString = replacedString.replace(matchResult.groups[0]?.value.orEmpty(), "_:B")
            }
            replacedString
        }.sorted().reduce { acc, s -> acc + "\n" + s }
    }

    @Test
    fun `test get record`() {
        val rdfHandler = RdfHandler(readFile("input.nt"), "https://memobase.ch/")

        val pair = rdfHandler.getRecord()

        val out = StringWriter()
        RDFDataMgr.write(out, pair.second, Lang.NTRIPLES)

        log.error(out.toString())

        val sortedOut = sort(out.toString().split("\n")).trim()

        assertThat(sortedOut)
            .isEqualTo(sort(readFile("recordOutput.nt").split("\n")).trim())

        assertThat(pair.first)
                .isEqualTo("https://memobase.ch/record/AFZ-IB_Becker_Audiovisuals_63")
    }

    @Test
    fun `test get record with activity and mechanism`() {
        val rdfHandler = RdfHandler(readFile("inputActivity.nt"), "https://memobase.ch/")

        val pair = rdfHandler.getRecord()

        val out = StringWriter()
        RDFDataMgr.write(out, pair.second, Lang.NTRIPLES)

        log.error(out.toString())

        val sortedOut = sort(out.toString().split("\n")).trim()

        assertThat(sortedOut)
            .isEqualTo(sort(readFile("recordActivityOutput.nt").split("\n")).trim())

        assertThat(pair.first)
            .isEqualTo("https://memobase.ch/record/AfZ-Bosshard-NL_Walter_Bosshard-504")
    }

    @Test
    fun `test get referenced non binary resources`() {
        val rdfHandler = RdfHandler(readFile("input.nt"), "https://memobase.ch/")
        val resources = rdfHandler.getReferencedNonBinaryResources()

        resources.sorted().forEachIndexed { index, uri ->
            assertThat(uri)
                    .isEqualTo(nonBinaryResourcesResult[index])
        }
    }

    private val nonBinaryResourcesResult: List<String> = listOf(
            "https://memobase.ch/instantiation/digital/AFZ-IB_Becker_Audiovisuals_63-1",
            "https://memobase.ch/instantiation/physical/AFZ-IB_Becker_Audiovisuals_63-0",
            "https://memobase.ch/institution/AFZ",
            "https://memobase.ch/institution/Memoriav",
            "https://memobase.ch/record/AFZ-IB_Becker_Audiovisuals_63",
            "https://memobase.ch/recordSet/BECKER"
    )

    @Test
    fun `test get instantiations`() {
        val rdfHandler = RdfHandler(readFile("input.nt"), "https://memobase.ch/")
        val list = rdfHandler.getInstantiations()

        list.forEachIndexed { index, pair ->
            val out = StringWriter()
            RDFDataMgr.write(out, pair.second, Lang.NTRIPLES)
            val sortedOut = sort(out.toString().split("\n")).trim()

            assertThat(sortedOut)
                .isEqualTo(sort(readFile("instantiationOutput$index.nt").split("\n")).trim())

            assertThat(pair.first)
                    .isEqualTo(uris[index])
        }
    }

    private val uris = listOf(
            "https://memobase.ch/instantiation/digital/AFZ-IB_Becker_Audiovisuals_63-1",
            "https://memobase.ch/instantiation/physical/AFZ-IB_Becker_Audiovisuals_63-0"
    )

    @Test
    fun `test get sftp locators`() {
        val rdfHandler = RdfHandler(readFile("inputSftp.nt"), "https://memobase.ch/")
        val pairs = rdfHandler.getSftpLocators()

        pairs.forEachIndexed { index, pair ->
            assertThat(pair)
                .isEqualTo(resultPairs[index])
        }
    }

    private val resultPairs: List<Pair<String, String?>> = listOf(
        Pair("https://memobase.ch/instantiation/digital/AFZ-IB_Becker_Audiovisuals_63-2", "/path/to/file/filename.jpg")
    )

    @Test
    fun `test replace sftp locators`() {
        val rdfHandler = RdfHandler(readFile("inputSftp.nt"), "https://memobase.ch/")
        val list = rdfHandler.getInstantiations()

        list.forEachIndexed { index, pair ->
            val out = StringWriter()
            RDFDataMgr.write(out, pair.second, Lang.NTRIPLES)
            val sortedOut = sort(out.toString().split("\n")).trim()

            assertThat(sortedOut)
                .isEqualTo(sort(readFile("replaceSftpLocatorOutput$index.nt").split("\n")).trim())

            assertThat(pair.first)
                .isEqualTo(uris2[index])
        }
    }

    @Test
    fun `test mime type extraction for binaries`() {
        val rdfHandler = RdfHandler(readFile("input.nt"), "https://memobase.ch/")
        val mimeType = rdfHandler.getMimeType("https://memobase.ch/instantiation/digital/AFZ-IB_Becker_Audiovisuals_63-1")
        assertThat(mimeType).isEqualTo("video/mpeg")
    }

    private val uris2 = listOf(
        "https://memobase.ch/instantiation/digital/AFZ-IB_Becker_Audiovisuals_63-1",
        "https://memobase.ch/instantiation/digital/AFZ-IB_Becker_Audiovisuals_63-2"
    )
}
