/*
 * fedora-ingest-service
 * Copyright (C) 2020 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.memobase

import java.io.Closeable
import java.time.Duration
import java.util.Properties
import org.apache.kafka.clients.consumer.ConsumerRecords
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger

class Consumer(props: Properties, topic: String) : Closeable {
    private val instance = KafkaConsumer<String, String>(props)
    private val log: Logger = LogManager.getLogger("FedoraIngestConsumer")

    init {
        instance.subscribe(listOf(topic))
    }

    fun fetchRecords(): ConsumerRecords<String, String> {
        log.info("Polling for new records.")
        return instance.poll(Duration.ofMillis(1000))
    }

    override fun close() {
        instance.close()
    }
}
