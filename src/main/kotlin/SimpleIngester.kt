/*
 * fedora-ingest-service
 * Copyright (C) 2020 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.memobase

import ch.memobase.fedora.FedoraClient
import ch.memobase.fedora.RdfContentTypes
import java.io.IOException
import java.net.URI
import java.net.URISyntaxException
import org.apache.logging.log4j.LogManager
import org.fcrepo.client.FcrepoOperationFailedException

class SimpleIngester(
    private val fedoraClient: FedoraClient
) {
    private val log = LogManager.getLogger("SimpleIngester")

    @Throws(FcrepoOperationFailedException::class, IOException::class, URISyntaxException::class)
    fun ingest(id: String, content: String) {
        fedoraClient.startTransaction().use { transaction ->
            log.info("Begin transaction to ingest single entity with uri $id.")
            transaction.createOrUpdateRdfResource(URI(id), content, RdfContentTypes.NTRIPLES)
            transaction.commit()
            log.info("Committed transaction for entity with uri $id.")
        }
    }
}
