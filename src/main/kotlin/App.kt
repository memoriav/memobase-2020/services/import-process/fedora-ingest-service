/*
 * fedora-ingest-service
 * Copyright (C) 2020 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.memobase

import kotlin.system.exitProcess
import org.apache.logging.log4j.LogManager

class App {
    companion object {
        private val log = LogManager.getLogger("FedoraIngestApp")
        @JvmStatic fun main(args: Array<String>) {
            try {
                val service = Service()
                service.use {
                    it.run()
                }
            } catch (ex: Exception) {
                log.error("Stopping application due to error: ${ex.localizedMessage}", ex)
                exitProcess(1)
            }
        }
    }
}
