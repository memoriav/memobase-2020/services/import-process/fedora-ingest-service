package org.memobase

import ch.memobase.rdf.EBUCORE
import ch.memobase.rdf.RDF
import ch.memobase.rdf.RICO
import java.io.ByteArrayInputStream
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.rdf.model.Statement
import org.apache.jena.rdf.model.impl.SelectorImpl
import org.apache.jena.rdf.model.impl.StatementImpl
import org.apache.jena.riot.Lang
import org.apache.jena.riot.RDFDataMgr
import org.apache.log4j.LogManager
import org.memobase.exceptions.MissingMimeTypeException

class RdfHandler(data: String, private val externalBaseUrl: String) {
    private val log = LogManager.getLogger("IngestRdfHandler")
    private val model = ModelFactory.createDefaultModel()

    init {
        RDFDataMgr.read(model, ByteArrayInputStream(data.toByteArray()), Lang.NTRIPLES)
    }

    fun getRecord(): Pair<String, Model> {
        val resultModel = ModelFactory.createDefaultModel()
        var uri = ""
        model.listSubjectsWithProperty(RDF.type, RICO.Record).forEach { resource ->
            resource.listProperties().forEach { statement ->
                if (statement.`object`.isAnon) {
                    val blankNode = statement.`object`.asResource()
                    if (blankNode.hasProperty(RDF.type, RICO.CreationRelation)) {
                        blankNode.listProperties(RICO.creationRelationHasTarget).forEach { targetStatement ->
                            // Adding agent statements connected to creation relations
                            resultModel.add(targetStatement.`object`.asResource().listProperties())
                        }
                    }
                    // Adding statements connected via Activity / Mechanism.
                    if (blankNode.hasProperty(RICO.resultsFrom)) {
                        blankNode.listProperties(RICO.resultsFrom).forEach { targetStatement ->
                            val values = targetStatement.`object`.asResource().listProperties()
                            values.forEach { activityStatement ->
                                if (activityStatement.predicate == RICO.performedBy) {
                                    val mechanism = activityStatement.`object`.asResource()
                                    resultModel.add(mechanism.listProperties())
                                }
                                resultModel.add(activityStatement)
                            }
                        }
                    }
                    // Adding blank node statements connected to record
                    resultModel.add(blankNode.listProperties())
                }
                // Adding record statements
                resultModel.add(statement)
            }
            uri = resource.uri
        }
        return Pair(uri, resultModel)
    }

    private fun isInternalNonBinaryResource(uri: String): Boolean {
        return uri.startsWith(externalBaseUrl) && !uri.endsWith("/${Service.BINARY_FILE_URI_PATH}")
    }

    fun getReferencedNonBinaryResources(): List<String> {
        return model.listStatements().filterKeep { statement ->
            statement.getObject().isURIResource
        }.mapWith { statement ->
            statement.getObject().asResource().uri
        }.filterKeep { uri ->
            isInternalNonBinaryResource(uri)
        }.toList().distinct().sorted()
    }

    fun getInstantiations(): List<Pair<String, Model>> {
        return model.listSubjectsWithProperty(RDF.type, RICO.Instantiation).mapWith { resource ->
            val model = ModelFactory.createDefaultModel()
            resource.listProperties().forEach { statement ->
                if (statement.`object`.isAnon) {
                    val blankNode = statement.`object`.asResource()
                    // Adding blank node statements connected to record
                    model.add(blankNode.listProperties())
                }
                model.add(statement)
            }
            if (resource.hasProperty(EBUCORE.locator)) {
                replaceSftpLocator(resource.uri, model)
            }
            Pair(resource.uri, model)
        }.toList()
    }

    private fun replaceSftpLocator(uri: String, model: Model) {
        var newStatement: Statement? = null
        val string: String? = null
        val replacementLocator = "$uri/${Service.BINARY_FILE_URI_PATH}"
        val removedStatement = model.listStatements(SelectorImpl(null, EBUCORE.locator, string)).mapWith {
            if (it.`object`.asLiteral().string.startsWith(Service.SFTP_PREFIX)) {
                newStatement = StatementImpl(it.subject, it.predicate, model.createLiteral(replacementLocator))
            }
            it
        }.nextOptional()
        removedStatement.let {
            if (it.isPresent && newStatement != null) {
                model.remove(it.get())
                model.add(newStatement)
            }
        }
        log.info("Replaced sftp locator with fedora url: $replacementLocator.")
    }

    fun getSftpLocators(): List<Pair<String, String>> {
        return model.listResourcesWithProperty(RDF.type, RICO.Instantiation).filterKeep {
            it.hasProperty(EBUCORE.locator) && it.getProperty(EBUCORE.locator).`object`.isLiteral
        }.filterKeep {
            it.getProperty(EBUCORE.locator).`object`.asLiteral().string.startsWith(Service.SFTP_PREFIX)
        }.mapWith {
            val url = it.getProperty(EBUCORE.locator).`object`.asLiteral().string
            Pair(it.uri, url.replace(Service.SFTP_PREFIX, ""))
        }.toList()
    }

    @Throws(MissingMimeTypeException::class)
    fun getMimeType(uri: String): String {
        val mimeTypes = model.getResource(uri).listProperties(EBUCORE.hasMimeType).mapWith {
            it.`object`.asLiteral().string
        }.toList()
        if (mimeTypes.size == 1) {
            return mimeTypes[0]
        } else {
            throw MissingMimeTypeException("No MimeType found for resource $uri")
        }
    }
}
