package org.memobase.exceptions

class MissingMimeTypeException(message: String) : Exception(message)
