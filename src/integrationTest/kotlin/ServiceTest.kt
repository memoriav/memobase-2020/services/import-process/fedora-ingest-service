/*
 * fedora-ingest-service
 * Copyright (C) 2019  Memobase
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.memobase

import ch.memobase.testing.EmbeddedKafkaExtension
import ch.memobase.testing.EmbeddedSftpServer
import com.beust.klaxon.Klaxon
import java.io.File
import java.io.FileInputStream
import java.time.Duration
import java.util.Properties
import java.util.stream.Stream
import org.apache.kafka.clients.admin.AdminClient
import org.apache.kafka.clients.admin.AdminClientConfig
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.header.internals.RecordHeader
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.kafka.common.serialization.StringSerializer
import org.apache.logging.log4j.LogManager
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

@ExtendWith(EmbeddedKafkaExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ServiceTest {
    private val log = LogManager.getLogger("LocalTestsLogger")

    private val sftpServer = EmbeddedSftpServer(22000, "user", "password")

    private val adminClient =
        AdminClient.create(mapOf(Pair(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:12345")))

    init {
        // setup embedded sftpServer with test data
        sftpServer.createDirectories(
            "/memobase/AFZ/BECKER/"
        )
        sftpServer.putFile(
            "/memobase/AFZ/BECKER/binary.txt",
            FileInputStream("src/integrationTest/resources/sftpData/binary.txt")
        )
    }

    private val inputProducer: KafkaProducer<String, String>
    private val reportConsumer: KafkaConsumer<String, String>

    init {
        val producerProps = Properties()
        producerProps.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:12345")
        producerProps.setProperty(ProducerConfig.CLIENT_ID_CONFIG, "input-producer")
        producerProps.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer::class.qualifiedName)
        producerProps.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer::class.qualifiedName)
        inputProducer = KafkaProducer(producerProps)

        val consumerProps = Properties()
        consumerProps.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:12345")
        consumerProps.setProperty(ConsumerConfig.CLIENT_ID_CONFIG, "report-consumer")
        consumerProps.setProperty(ConsumerConfig.GROUP_ID_CONFIG, "test-group-1")
        consumerProps.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer::class.qualifiedName)
        consumerProps.setProperty(
            ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
            StringDeserializer::class.qualifiedName
        )
        consumerProps.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest")
        reportConsumer = KafkaConsumer(consumerProps)
        reportConsumer.subscribe(listOf("test-ingest-reporting"))
    }

    // TODO: Test works with local docker
    // docker run --rm -p 8080:8080 --name=fcrepo5 fcrepo/fcrepo:5.1.0
    @ParameterizedTest
    @MethodSource("kafkaTests")
    fun testRun(params: TestParams) {
        for (inputFileName in params.inputFileNames) {
            val inputFile = File("src/integrationTest/resources/${params.count}/$inputFileName")
            val id = inputFile.nameWithoutExtension
            inputProducer.send(
                    ProducerRecord(
                            "test-ingest-in",
                            null,
                            "https://memobase.ch/record/$id",
                            inputFile.readText(),
                            params.headers
                    )
            )
        }
        val expectedRecordCount = params.expectedIngestReports.size

        val service = Service("test${params.count}.yml")

        val totalConsumerRecords = mutableListOf<ConsumerRecord<String, String>>()
        while (totalConsumerRecords.size != expectedRecordCount) {
            service.processRecords()
            var result = reportConsumer.poll(Duration.ofMillis(10))
            if (result.count() > 0) {
                totalConsumerRecords.addAll(result.asIterable())
            }
        }

        assertThat(totalConsumerRecords)
            .size().isEqualTo(expectedRecordCount)

        val receivedReport = Klaxon().parse<Report>(totalConsumerRecords[0].value())
        assertNotNull(receivedReport)
        assertThat(receivedReport).isEqualTo(params.expectedIngestReports[0])
    }

    private fun kafkaTests() = Stream.of(
        TestParams(
            1,
            listOf(
                RecordHeader("recordSetId", "AFZ-IB_Becker_Audiovisuals".toByteArray()),
                RecordHeader("sessionId", "long-session-id-1234567890".toByteArray()),
                RecordHeader("shortSessionId", "1234567890".toByteArray()),
                RecordHeader("institutionId", "AFZ".toByteArray())
            ),
            listOf(
                "AFZ-IB_Becker_Audiovisuals_63.nt"
            ),
            listOf(
                Report(
                    id = "https://memobase.ch/record/AFZ-IB_Becker_Audiovisuals_63",
                    status = "SUCCESS",
                    message = "Ingested resource https://memobase.ch/record/AFZ-IB_Becker_Audiovisuals_63."
                )
            )
        )
    )
}
